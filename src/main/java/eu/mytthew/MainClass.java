package eu.mytthew;

public class MainClass {
	public static void main(String[] args) {
		DoorFactory doorFactory = new DoorFactory();
		doorFactory.whiteDoor();
		doorFactory.brownDoor();
	}
}
