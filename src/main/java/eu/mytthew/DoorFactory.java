package eu.mytthew;

public class DoorFactory {
	public Door whiteDoor() {
		Door door = new Door();
		door.setColor("WHITE");
		door.setLeft(true);
		door.setMaxAngle(90);
		door.setLock(false);
		door.setWindow(true);
		return door;
	}

	public Door brownDoor() {
		Door door = new Door();
		door.setColor("BROWN");
		door.setLeft(false);
		door.setMaxAngle(135.5);
		door.setLock(true);
		door.setWindow(false);
		return door;
	}
}
